# datadiff_example

Example code to use a package to do diff on data
 
* Adapted from:

    * https://github.com/alan-turing-institute/datadiff/blob/master/vignettes/datadiff.Rmd
   
* Installation:
    
    * install.packages("devtools")
    * devtools::install_github("alan-turing-institute/datadiff") 
    
